#!/bin/bash
HELP="Usage: add_wg_client.sh <name> [ip]"
if [ -n "$1" ] && [ -n "$2" ];
then
    NAME=$1
    PEERIP=$2
else
    echo ${HELP}
    exit;
fi

echo "User: "${NAME}
echo "IP: "${PEERIP}
echo -n "Continue[y|Y]:"
read Key
case $Key in
"y"|"Y" ) ;;
*       )
        echo "Exit"
        exit;
esac

USER_FILE=$NAME.conf
CONFIG_FILE=/etc/wireguard/wg0.conf

wg genkey | tee /tmp/x_privatekey | wg pubkey | tee /tmp/x_publickey \
&& printf "\n[Peer]\nPublicKey = " >> $CONFIG_FILE && cat /tmp/x_publickey >> $CONFIG_FILE \
&& printf "AllowedIPs = $PEERIP/32\n" >> $CONFIG_FILE \
&& printf "[Interface]\nPrivateKey = " > $USER_FILE && cat /tmp/x_privatekey >> $USER_FILE \
&& printf "Address = $PEERIP/32\n\n" >> $USER_FILE \
&& printf "[Peer]\nPublicKey = " >> $USER_FILE \
&& cat /etc/wireguard/publickey >> $USER_FILE \
&& printf "AllowedIPs = 10.0.0.0/24\nEndpoint = dogit-studios.ru:51820\nPersistentKeepalive = 20" >> $USER_FILE \
&& rm /tmp/x_p* && systemctl restart wg-quick@wg0.service