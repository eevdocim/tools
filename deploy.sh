#!/bin/bash
HELP="Usage: deploy.sh <install|update> name_project framework|cms|dogit"
WWW_PATH=/var/www/html
if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    echo ${HELP}
    exit;
fi
case $1 in
"install"|"update")
        INSTALLER=$1
        ;;
*       )
        echo ${HELP}
        exit;
        ;;
esac
PROJECT=$2
case $3 in
"laravel"|"dogit")
        ENV=$3
        ;;
*       )
        echo ${HELP}
        exit;
        ;;
esac
echo "Type installer: "${INSTALLER}
echo "Project: "${PROJECT}
echo "Framework|CMS: "${ENV}
echo -n "Continue[y|Y]:"
read Key
case $Key in
"y"|"Y" ) ;;
*       )
        echo "Exit"
        exit;
esac
case $ENV in
"laravel")
        if [ -d ${WWW_PATH}/${PROJECT} ] && [ ${INSTALLER} = "update" ]; 
        then
                echo "Start update"
                cd ${WWW_PATH}
                sudo chown -R root:root ${PROJECT}
                cd ${WWW_PATH}/${PROJECT}
                sudo git checkout .
                sudo git pull origin main
                cd ${WWW_PATH}
                sudo chown -R www-data:www-data ${PROJECT}
                sudo chmod -R 775 ${PROJECT}
                cd ${WWW_PATH}/${PROJECT}
                composer install
                php artisan migrate
                php artisan optimize
		npm install
                npm run dev
                echo "Finished update"
        else
            echo "Folber not found"
        fi
        ;;
"dogit")
        if [ -d ${WWW_PATH}/${PROJECT} ] && [ ${INSTALLER} = "update" ]; 
        then
                echo "Start update"
                cd ${WWW_PATH}
                sudo chown -R root:root ${PROJECT}
                cd ${WWW_PATH}/${PROJECT}
                sudo git checkout .
                sudo git pull origin main
                cd ${WWW_PATH}
                sudo chown -R www-data:www-data ${PROJECT}
                sudo chmod -R 775 ${PROJECT}
                echo "Finished update"
        else
            echo "Folber not found"
        fi
        ;;
*       )
        echo ${HELP}
        exit;
        ;;
esac
