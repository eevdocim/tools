#!/bin/bash
apt update
apt install vsftpd -y
conf="/etc/vsftpd.conf"
backup_conf="/etc/vsftpd.conf.orig"
if [ -e $backup_conf ]
then
echo "Резервной копия конфигурации существует"
else
echo "Создание резервной копии конфигурации"
cp $conf $backup_conf
fi

sed -i 's/#write_enable=YES/write_enable=YES/g' $conf
sed -i 's/#local_umask=022/local_umask=022/g' $conf
sed -i 's/#ascii_upload_enable=YES/ascii_upload_enable=YES/g' $conf
sed -i 's/#ascii_download_enable=YES/ascii_download_enable=YES/g' $conf
sed -i 's/#utf8_filesystem=YES/utf8_filesystem=YES/g' $conf

systemctl restart vsftpd

user="ftpuser"
if id "$user" &>/dev/null; then
    echo 'Пользователь ftpuser существует'
else
    useradd "$user"
    mkhomedir_helper "$user"
    passwd "$user"
    echo 'Пользователь ftpuser создан'
fi