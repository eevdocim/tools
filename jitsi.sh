#!/bin/bash
sudo apt update
sudo apt install apt-transport-https jitsi-meet
sudo apt install build-essential libtool maven
sudo systemctl stop prosody jitsi-videobridge2 jicofo
git clone https://github.com/sctplab/usrsctp.git
git clone https://github.com/jitsi/jitsi-sctp
mv ./usrsctp ./jitsi-sctp/usrsctp/
cd ./jitsi-sctp
mvn package -DbuildSctp -DbuildNativeWrapper -DdeployNewJnilib -DskipTests
sudo cp /home/test/jitsi/jitsi-sctp/target/jitsi-sctp-1.0-SNAPSHOT.jar  /usr/share/jitsi-videobridge/lib/jitsi-sctp-1.0-SNAPSHOT.jar
sudo systemctl start prosody jitsi-videobridge2 jicofo
sudo systemctl status jitsi-videobridge2
sudo systemctl status coturn