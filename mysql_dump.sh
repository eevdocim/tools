#/bin/bash

# user & password
USER=root
PASS=

#host
HOST=127.0.0.1

#dir to backup
DIR=./

NOW=$(date +"%Y-%m-%d")
 
for nameDB in db_name
do
    FILE=$DIR/$nameDB-$NOW.sql.gz
    mysqldump --databases --no-tablespaces --skip-comments -h$HOST -u$USER -p$PASS $nameDB | gzip > $FILE
done

exit 0