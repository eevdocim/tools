#/bin/bash

# user & password
USER=root
PASS=

#host
HOST=127.0.0.1

#dir to backup
DIR=./

NOW=$(date +"%Y-%m-%d")

for nameDB in db_name
do
    FILE=$DIR/$nameDB-$NOW.dmp
    PGPASSWORD=$PASS pg_dump -h $HOST -U $USER -c $nameDB > $FILE
done

exit 0