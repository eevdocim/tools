wget -q -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo gpg --dearmor -o /etc/apt/keyrings/pgsql.gpg
sudo sh -c 'echo "deb [signed-by=/etc/apt/keyrings/pgsql.gpg]  http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
apt-get update
apt-get -y install postgresql