# add-apt-repository ppa:ondrej/php #for ubuntu

sudo wget -qO /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg # for Raspberry Pi
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list # for Raspberry Pi

apt update
apt install apache2 php-fpm libapache2-mod-fcgid php libapache2-mod-php -y
a2enmod actions
a2enconf php-fpm
a2enmod proxy
a2enmod proxy_fcgi
ports_apache="/etc/apache2/ports.conf"
backup_ports_apache="/etc/apache2/ports.conf.orig"
conf_apache="/etc/apache2/sites-available/000-default.conf"
backup_conf_apache="/etc/apache2/sites-available/000-default.conf.orig"

if [ -e $backup_ports_apache ]
then
echo "Резервной копия портов существует"
else
echo "Создание резервной копии портов"
cp $ports_apache $backup_ports_apache
fi

if [ -e $backup_conf_apache ]
then
echo "Резервной копия конфигурации существует"
else
echo "Создание резервной копии конфигурации"
cp $conf_apache $backup_conf_apache
fi

sed -i 's/Listen 80/Listen 127.0.0.1:8080/g' $ports_apache
sed -i 's/*:80/127.0.0.1:8080/g' $conf_apache

systemctl restart apache2
apt install nginx -y
default="/etc/nginx/sites-available/default"
backup_default="/etc/nginx/sites-available/default.orig"

if [ -e $backup_default ]
then
echo "Резервной копия конфигурации существует"
else
echo "Создание резервной копии конфигурации"
cp $default $backup_default
fi

cp ./nginx.conf /etc/nginx/sites-available/default

echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php



















