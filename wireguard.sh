#!/bin/bash
apt update
apt install -y wireguard resolvconf iptables
wg genkey | tee /etc/wireguard/privatekey | wg pubkey | tee /etc/wireguard/publickey
chmod 600 /etc/wireguard/privatekey

CONFIG_FILE=/etc/wireguard/wg0.conf

printf "[Interface]\nPrivateKey = " > $CONFIG_FILE \
&& cat /etc/wireguard/privatekey >> $CONFIG_FILE \
&& printf "Address = 10.0.0.1/24\n" >> $CONFIG_FILE \
&& printf "ListenPort = 51820\n" >> $CONFIG_FILE \
&& printf "PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE\n" >> $CONFIG_FILE \
&& printf "PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE\n" >> $CONFIG_FILE
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
systemctl enable wg-quick@wg0.service
systemctl start wg-quick@wg0.service
systemctl status wg-quick@wg0.service